git clone https://github.com/emscripten-core/emsdk.git
pushd emsdk
call emsdk install latest
call emsdk activate latest
call emsdk_env.bat

REM Fix for emrun not having psutil available.
%EMSDK_PYTHON% ..\get-pip.py
%EMSDK_PYTHON% -m pip install psutil