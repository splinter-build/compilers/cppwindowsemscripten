local emsdkInstalled = spl_get("path_exists", spl_get("mod_path", "emsdk/upstream/bin"))
if emsdkInstalled == false then
	print("Emscripten not installed correctly. emsdk/upstream/bin not available")
	return false
end

-- detect node JS
local nodePaths = spl_get("path_dirs", spl_get("mod_path", "emsdk/node"))
if #nodePaths == 0 then print("Emscripten node not detected. Skipping.") return false end
local nodePath = "emsdk/node/" .. nodePaths[1] .. "/bin"

-- detect python
local pythonPaths = spl_get("path_dirs", spl_get("mod_path", "emsdk/python"))
if #pythonPaths == 0 then print("Emscripten python not detected. Skipping.") return false end
local pythonPath = "emsdk/python/" .. pythonPaths[1]

-- detect java
local javaPaths = spl_get("path_dirs", spl_get("mod_path", "emsdk/java"))
if #javaPaths == 0 then print("Emscripten java not detected. Skipping.") return false end
local javaPath = "emsdk/java/" .. javaPaths[1]

spl_add_compiler { 
	name = "emscripten", 
	type = "cpp_compiler", 
	host_os = "windows", 
	target = "emscripten", 
	prio = 100, 
	pretty_name = "Emscripten (Windows)", 
	path={
		spl_get("mod_path", nodePath),
		spl_get("mod_path", "emsdk"),
		spl_get("mod_path", "emsdk/upstream/emscripten"),
	},
	env={
		EMSDK = spl_get("mod_path", "emsdk"),
		EM_CONFIG = spl_get("mod_path", "emsdk", ".emscripten"),
		EMSDK_PYTHON = spl_get("mod_path", pythonPath, "python.exe"),
		EMSDK_NODE = spl_get("mod_path", nodePath, "node.exe"),
		JAVA_HOME = spl_get("mod_path", javaPath),
	},
	compiler = {
		module = "compiler.cpp.windows.emscripten",
		cc = "emcc", 
		cxx = "em++",
		link = "em++",	
		lib = "emar", 
		convention = "gcc_emscripten", 
		type = "gcc", 
	}
}

spl_module({
	description = "C++ Emscripten Compiler for Windows"
})